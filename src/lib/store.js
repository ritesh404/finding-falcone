import K from "fp-kudojs";
import * as R from "ramda";
import { createStore, applyMiddleware, compose } from "redux";
import createSagaMiddleware from "redux-saga";

import rootSaga from "./sagas";
import handlers from "./reducerHandlers";

const { Maybe, caseOf } = K;
const { Nothing, fromNullable } = Maybe;

// Saga middleware to handle async calls
const sagaMiddleware = createSagaMiddleware();

export const INITIAL_STATE = {
  token: Nothing(),
  selectedDestination: Nothing(),
  destination1: Nothing(),
  destination2: Nothing(),
  destination3: Nothing(),
  destination4: Nothing(),
  planets: [],
  vehicles: [],
  showError: false,
  isFetchingPlanets: false,
  isFetchingVehicles: false,
  isFetchingToken: false,
  isFetchingResult: false,
  result: Nothing()
};

export const STATE_KEYS = R.compose(
  keys => R.zipObj(keys, keys),
  R.keys
)(INITIAL_STATE);

// We check if the action type exists within the handler object.
// If it does, We apply the function value mapped to the action type to the current state and payload
// If no handler exist we simply return the current state
// This produces the new state. This is our single reducer
const Reducer = (state = INITIAL_STATE, action) =>
  caseOf(
    {
      Just: actionFn => actionFn(state, action.payload),
      Nothing: () => state
    },
    fromNullable(handlers[action.type])
  );

const composeEnhancers =
  typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose;

const enhancer = composeEnhancers(applyMiddleware(sagaMiddleware));

const store = createStore(Reducer, enhancer);

sagaMiddleware.run(rootSaga);

export default store;
