export const PORTAL_ID = "portal";

export const VEHICLES = "VEHICLES";
export const PLANETS = "PLANETS";
export const TOKEN = "TOKEN";
export const RESULT = "RESULT";

// endpoints
export const VEHICLES_ENDPOINT = "https://findfalcone.herokuapp.com/vehicles";
export const PLANETS_ENDPOINT = "https://findfalcone.herokuapp.com/planets";
export const TOKEN_ENDPOINT = "https://findfalcone.herokuapp.com/token";
export const FIND_ENDPOINT = "https://findfalcone.herokuapp.com/find";

//Action types
export const FETCH_VEHICLES = "FETCH_VEHICLES";
export const FETCH_TOKEN = "FETCH_TOKEN";
export const FETCH_PLANETS = "FETCH_PLANETS";
export const FETCH_RESULT = "FETCH_RESULT";
export const SET_DESTINATION = "SET_DESTINATION";
export const SET_SELECTED_DESTINATION = "SET_SELECTED_DESTINATION";
export const TOKEN_FETCH_SUCCEEDED = "TOKEN_FETCH_SUCCEEDED";
export const VEHICLES_FETCH_SUCCEEDED = "VEHICLES_FETCH_SUCCEEDED";
export const API_FETCH_FAILED = "API_FETCH_FAILED";
export const PLANETS_FETCH_SUCCEEDED = "PLANETS_FETCH_SUCCEEDED";
export const RESULT_FETCH_SUCCEEDED = "RESULT_FETCH_SUCCEEDED";
