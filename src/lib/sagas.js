import * as R from "ramda";
import K from "fp-kudojs";
import { put, takeLatest, all, select } from "redux-saga/effects";
import {
  _vehicleFetchSucceeded,
  _planetsFetchSucceeded,
  _tokenFetchSucceeded,
  _resultFetchSucceeded,
  _apiFetchFailed
} from "./actions";
import {
  VEHICLES,
  PLANETS,
  TOKEN,
  RESULT,
  VEHICLES_ENDPOINT,
  PLANETS_ENDPOINT,
  TOKEN_ENDPOINT,
  FIND_ENDPOINT,
  FETCH_VEHICLES,
  FETCH_TOKEN,
  FETCH_PLANETS,
  FETCH_RESULT
} from "./constants";
import { STATE_KEYS } from "./store";

function* fetchVehicles() {
  try {
    const vehicles = yield fetch(VEHICLES_ENDPOINT).then(response =>
      response.json()
    );
    yield put(_vehicleFetchSucceeded(vehicles));
  } catch (error) {
    yield put(_apiFetchFailed({ error, api: VEHICLES }));
  }
}

function* fetchPlanets() {
  try {
    const planets = yield fetch(PLANETS_ENDPOINT).then(response =>
      response.json()
    );
    yield put(_planetsFetchSucceeded(planets));
  } catch (error) {
    yield put(_apiFetchFailed({ error, api: PLANETS }));
  }
}

function* fetchToken() {
  try {
    const tokenResponse = yield fetch(TOKEN_ENDPOINT, {
      method: "POST",
      headers: {
        Accept: "application/json"
      }
    }).then(response => response.json());
    yield put(_tokenFetchSucceeded(tokenResponse.token));
  } catch (error) {
    yield put(_apiFetchFailed({ error, api: TOKEN }));
  }
}

const getAllDestinations = state => [
  state.destination1,
  state.destination2,
  state.destination3,
  state.destination4
];

const extractPropFromDestinations = propName =>
  R.compose(
    R.map(R.prop(propName)),
    K.Maybe.catMaybes
  );

const searchRequestBody = (token, vehicle_names, planet_names) =>
  JSON.stringify({
    token,
    planet_names,
    vehicle_names
  });

function* fetchResult() {
  try {
    const state = yield select(
      R.pick([
        STATE_KEYS.token,
        STATE_KEYS.destination1,
        STATE_KEYS.destination2,
        STATE_KEYS.destination3,
        STATE_KEYS.destination4
      ])
    );
    const destinations = getAllDestinations(state);
    const vehicle_names = extractPropFromDestinations("vehicle")(destinations);
    const planet_names = extractPropFromDestinations("planet")(destinations);
    const body = searchRequestBody(
      state.token.getValue(),
      planet_names,
      vehicle_names
    );

    const resultResponse = yield fetch(FIND_ENDPOINT, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body
    }).then(response => response.json());
    yield put(_resultFetchSucceeded(resultResponse));
  } catch (error) {
    yield put(_apiFetchFailed({ error, api: RESULT }));
  }
}

function* actionWatcher() {
  yield [
    takeLatest(FETCH_VEHICLES, fetchVehicles),
    takeLatest(FETCH_TOKEN, fetchToken),
    takeLatest(FETCH_PLANETS, fetchPlanets),
    takeLatest(FETCH_RESULT, fetchResult)
  ];
}

export default function* rootSaga() {
  yield all([actionWatcher()]);
}
