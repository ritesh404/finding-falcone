import createBrowserHistory from "history/createBrowserHistory";

// Create a history object and export it. Use it for navigation
const appHistory = createBrowserHistory();
export default appHistory;
