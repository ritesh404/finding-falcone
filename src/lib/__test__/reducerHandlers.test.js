import K from "fp-kudojs";
import * as TestData from "../../testData";
import { INITIAL_STATE } from "../store";
import * as C from "../constants";
import {
  SET_DESTINATION,
  SET_SELECTED_DESTINATION,
  TOKEN_FETCH_SUCCEEDED,
  VEHICLES_FETCH_SUCCEEDED,
  PLANETS_FETCH_SUCCEEDED,
  FETCH_RESULT,
  FETCH_TOKEN,
  FETCH_PLANETS,
  FETCH_VEHICLES,
  RESULT_FETCH_SUCCEEDED,
  API_FETCH_FAILED
} from "../reducerHandlers";

const {
  Maybe: { Just, Nothing }
} = K;

describe("Reducer Handlers", () => {
  it("SET_DESTINATION", () => {
    const destinationId = "destination1";
    const planet = "Donlon";
    const vehicle = "Space Pod";
    const payload = { destinationId, planet, vehicle };
    const newState = SET_DESTINATION(INITIAL_STATE, payload);
    expect(newState).toEqual({
      ...INITIAL_STATE,
      [destinationId]: Just({ planet, vehicle })
    });
  });

  it("SET_SELECTED_DESTINATION", () => {
    const destinationId = "destination1";
    const newState = SET_SELECTED_DESTINATION(INITIAL_STATE, destinationId);
    expect(newState).toEqual({
      ...INITIAL_STATE,
      selectedDestination: destinationId
    });
  });

  it("TOKEN_FETCH_SUCCEEDED", () => {
    const token = "token1234";
    const newState = TOKEN_FETCH_SUCCEEDED(INITIAL_STATE, token);
    expect(newState).toEqual({
      ...INITIAL_STATE,
      token: Just(token)
    });
  });

  it("VEHICLES_FETCH_SUCCEEDED", () => {
    const vehicles = TestData.vehicles;
    const newState = VEHICLES_FETCH_SUCCEEDED(INITIAL_STATE, vehicles);
    expect(newState).toEqual({
      ...INITIAL_STATE,
      isFetchingVehicles: false,
      vehicles
    });
  });

  it("PLANETS_FETCH_SUCCEEDED", () => {
    const planets = TestData.planets;
    const newState = PLANETS_FETCH_SUCCEEDED(INITIAL_STATE, planets);
    expect(newState).toEqual({
      ...INITIAL_STATE,
      isFetchingPlanets: false,
      planets
    });
  });

  it("FETCH_RESULT", () => {
    const newState = FETCH_RESULT(INITIAL_STATE);
    expect(newState).toEqual({
      ...INITIAL_STATE,
      isFetchingResult: true
    });
  });

  it("FETCH_TOKEN", () => {
    const newState = FETCH_TOKEN(INITIAL_STATE);
    expect(newState).toEqual({
      ...INITIAL_STATE,
      isFetchingToken: true
    });
  });

  it("FETCH_VEHICLES", () => {
    const newState = FETCH_VEHICLES(INITIAL_STATE);
    expect(newState).toEqual({
      ...INITIAL_STATE,
      isFetchingVehicles: true
    });
  });

  it("FETCH_PLANETS", () => {
    const newState = FETCH_PLANETS(INITIAL_STATE);
    expect(newState).toEqual({
      ...INITIAL_STATE,
      isFetchingPlanets: true
    });
  });

  it("RESULT_FETCH_SUCCEEDED", () => {
    const newState = RESULT_FETCH_SUCCEEDED(
      INITIAL_STATE,
      TestData.resultSuccess
    );
    expect(newState).toEqual({
      ...INITIAL_STATE,
      isFetchingResult: false,
      result: Just(TestData.resultSuccess)
    });
  });

  it("VEHICLES API_FETCH_FAILED", () => {
    const newState = API_FETCH_FAILED(INITIAL_STATE, { api: C.VEHICLES });
    expect(newState).toEqual({
      ...INITIAL_STATE,
      isFetchingVehicles: false,
      showError: true
    });
  });

  it("PLANETS API_FETCH_FAILED", () => {
    const newState = API_FETCH_FAILED(INITIAL_STATE, { api: C.PLANETS });
    expect(newState).toEqual({
      ...INITIAL_STATE,
      isFetchingPlanets: false,
      showError: true
    });
  });

  it("TOKEN API_FETCH_FAILED", () => {
    const newState = API_FETCH_FAILED(INITIAL_STATE, { api: C.TOKEN });
    expect(newState).toEqual({
      ...INITIAL_STATE,
      isFetchingToken: false,
      showError: true
    });
  });

  it("RESULT API_FETCH_FAILED", () => {
    const newState = API_FETCH_FAILED(INITIAL_STATE, { api: C.RESULT });
    expect(newState).toEqual({
      ...INITIAL_STATE,
      isFetchingResult: false,
      showError: true
    });
  });
});
