import K from "fp-kudojs";
import { renderMaybe, findWithName } from "../common";
import * as TestData from "../../testData";

const {
  Maybe: { Just, Nothing }
} = K;

describe("Common", () => {
  describe("renderMaybe", () => {
    it('should run a render function if its a "Just" value', () => {
      const renderFn = K.id;
      const textValue = "text value";
      const justValue = Just(textValue);

      expect(renderMaybe(renderFn, justValue)).toEqual(textValue);
    });

    it('should return a null if it is a "Nothing"', () => {
      const renderFn = K.id;
      const justValue = Nothing();

      expect(renderMaybe(renderFn, justValue)).toEqual(null);
    });
  });

  describe("findWithName", () => {
    it('should return "Just" item that matches the name', () => {
      const vehicleName = "Space pod";
      expect(findWithName(vehicleName, TestData.vehicles)).toEqual(
        Just({
          name: "Space pod",
          total_no: 2,
          max_distance: 200,
          speed: 2
        })
      );
    });

    it('should return "Nothing" if item is not present', () => {
      const vehicleName = "Sea pod";
      expect(findWithName(vehicleName, TestData.vehicles)).toEqual(Nothing());
    });
  });
});
