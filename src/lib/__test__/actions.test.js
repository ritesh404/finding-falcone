import * as C from "../constants";
import {
  _setDestination,
  _setSelectedDestination,
  _fetchToken,
  _fetchResult,
  _fetchVehicles,
  _fetchPlanets,
  _tokenFetchSucceeded,
  _vehicleFetchSucceeded,
  _apiFetchFailed,
  _planetsFetchSucceeded,
  _resultFetchSucceeded
} from "../actions";
import * as TestData from "../../testData";

describe("Actions", () => {
  it("should create an action to set a destination", () => {
    const destinationId = "destination1";
    const planet = "Donlon";
    const vehicle = "Pod";
    const expectedAction = {
      type: C.SET_DESTINATION,
      payload: { destinationId, planet, vehicle }
    };
    expect(_setDestination(destinationId, planet, vehicle)).toEqual(
      expectedAction
    );
  });

  it("should create an action to set selected destination", () => {
    const destinationId = "destination1";
    const expectedAction = {
      type: C.SET_SELECTED_DESTINATION,
      payload: destinationId
    };
    expect(_setSelectedDestination(destinationId)).toEqual(expectedAction);
  });

  it("should create an action to fetch token", () => {
    const expectedAction = {
      type: C.FETCH_TOKEN
    };
    expect(_fetchToken()).toEqual(expectedAction);
  });

  it("should create an action to search result", () => {
    const expectedAction = {
      type: C.FETCH_RESULT
    };
    expect(_fetchResult()).toEqual(expectedAction);
  });

  it("should create an action to fetch vehicles", () => {
    const expectedAction = {
      type: C.FETCH_VEHICLES
    };
    expect(_fetchVehicles()).toEqual(expectedAction);
  });

  it("should create an action to fetch planets", () => {
    const expectedAction = {
      type: C.FETCH_PLANETS
    };
    expect(_fetchPlanets()).toEqual(expectedAction);
  });

  it("should create an action for successful fetch of token", () => {
    const token = "token";
    const expectedAction = {
      type: C.TOKEN_FETCH_SUCCEEDED,
      payload: token
    };
    expect(_tokenFetchSucceeded(token)).toEqual(expectedAction);
  });

  it("should create an action for successful fetch of vehicles", () => {
    const expectedAction = {
      type: C.VEHICLES_FETCH_SUCCEEDED,
      payload: TestData.vehicles
    };
    expect(_vehicleFetchSucceeded(TestData.vehicles)).toEqual(expectedAction);
  });

  it("should create an action for successful fetch of planets", () => {
    const expectedAction = {
      type: C.PLANETS_FETCH_SUCCEEDED,
      payload: TestData.planets
    };
    expect(_planetsFetchSucceeded(TestData.planets)).toEqual(expectedAction);
  });

  it("should create an action for successful fetch of search result", () => {
    const expectedAction = {
      type: C.RESULT_FETCH_SUCCEEDED,
      payload: TestData.resultSuccess
    };
    expect(_resultFetchSucceeded(TestData.resultSuccess)).toEqual(
      expectedAction
    );
  });

  it("should create an action for failed api response", () => {
    const expectedAction = {
      type: C.API_FETCH_FAILED,
      payload: "error"
    };
    expect(_apiFetchFailed("error")).toEqual(expectedAction);
  });
});
