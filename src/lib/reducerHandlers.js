import K from "fp-kudojs";
import appHistory from "./history";
import { VEHICLES, PLANETS, TOKEN, RESULT } from "./constants";

const { Maybe } = K;
const { Just, Nothing } = Maybe;

// NOTE: We have a function for each action and add it to the handlers array
// using the handlers array we create a single reducer function in store.js.
// We do this by keeping the name of the function same as the action
export const SET_DESTINATION = (state, { destinationId, planet, vehicle }) => {
  const newState = { ...state };
  newState[destinationId] = Just({ planet, vehicle });

  return newState;
};

export const SET_SELECTED_DESTINATION = (state, destination) => ({
  ...state,
  selectedDestination: destination
});

export const TOKEN_FETCH_SUCCEEDED = (state, token) => ({
  ...state,
  token: Just(token),
  isFetchingToken: false
});

export const VEHICLES_FETCH_SUCCEEDED = (state, vehicles) => ({
  ...state,
  isFetchingVehicles: false,
  vehicles
});

export const PLANETS_FETCH_SUCCEEDED = (state, planets) => ({
  ...state,
  isFetchingPlanets: false,
  planets
});

export const FETCH_RESULT = state => ({
  ...state,
  isFetchingResult: true
});

export const FETCH_TOKEN = state => ({
  ...state,
  isFetchingToken: true
});

export const FETCH_PLANETS = state => ({
  ...state,
  isFetchingPlanets: true
});

export const FETCH_VEHICLES = state => ({
  ...state,
  isFetchingVehicles: true
});

export const RESULT_FETCH_SUCCEEDED = (state, result) => {
  appHistory.push("/result");
  const newState = resetDestinations(state);
  return {
    ...newState,
    isFetchingResult: false,
    result: Just(result)
  };
};

export const API_FETCH_FAILED = (state, payload) => {
  const newState = {
    ...state,
    showError: true
  };
  if (payload.api === VEHICLES) {
    newState.isFetchingVehicles = false;
  }
  if (payload.api === PLANETS) {
    newState.isFetchingPlanets = false;
  }
  if (payload.api === TOKEN) {
    newState.isFetchingToken = false;
  }
  if (payload.api === RESULT) {
    newState.isFetchingResult = false;
  }
  return newState;
};

export const resetDestinations = state => ({
  ...state,
  selectedDestination: Nothing(),
  destination1: Nothing(),
  destination2: Nothing(),
  destination3: Nothing(),
  destination4: Nothing()
});

// Handlers: an object with the functions mapped to action names
const handlers = {
  SET_DESTINATION,
  SET_SELECTED_DESTINATION,
  FETCH_TOKEN,
  FETCH_PLANETS,
  FETCH_VEHICLES,
  TOKEN_FETCH_SUCCEEDED,
  VEHICLES_FETCH_SUCCEEDED,
  PLANETS_FETCH_SUCCEEDED,
  FETCH_RESULT,
  RESULT_FETCH_SUCCEEDED,
  API_FETCH_FAILED
};

export default handlers;
