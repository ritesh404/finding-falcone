import {
  SET_DESTINATION,
  SET_SELECTED_DESTINATION,
  FETCH_TOKEN,
  FETCH_RESULT,
  FETCH_VEHICLES,
  FETCH_PLANETS,
  TOKEN_FETCH_SUCCEEDED,
  VEHICLES_FETCH_SUCCEEDED,
  API_FETCH_FAILED,
  PLANETS_FETCH_SUCCEEDED,
  RESULT_FETCH_SUCCEEDED
} from "./constants";

export const _setDestination = (destinationId, planet, vehicle) => ({
  type: SET_DESTINATION,
  payload: { destinationId, planet, vehicle }
});

export const _setSelectedDestination = destinationId => ({
  type: SET_SELECTED_DESTINATION,
  payload: destinationId
});

export const _fetchToken = () => ({
  type: FETCH_TOKEN
});

export const _fetchResult = () => ({
  type: FETCH_RESULT
});

export const _fetchVehicles = () => ({
  type: FETCH_VEHICLES
});

export const _fetchPlanets = () => ({
  type: FETCH_PLANETS
});

export const _tokenFetchSucceeded = token => ({
  type: TOKEN_FETCH_SUCCEEDED,
  payload: token
});

export const _vehicleFetchSucceeded = vehicles => ({
  type: VEHICLES_FETCH_SUCCEEDED,
  payload: vehicles
});

export const _apiFetchFailed = error => ({
  type: API_FETCH_FAILED,
  payload: error
});

export const _planetsFetchSucceeded = planets => ({
  type: PLANETS_FETCH_SUCCEEDED,
  payload: planets
});

export const _resultFetchSucceeded = results => ({
  type: RESULT_FETCH_SUCCEEDED,
  payload: results
});
