import K from "fp-kudojs";
import * as R from "ramda";

export const renderMaybe = (renderFn, maybe) =>
  K.caseOf(
    {
      Just: renderFn,
      Nothing: () => null
    },
    maybe
  );

export const findWithName = (name, list) =>
  R.compose(
    K.Maybe.fromNullable,
    R.find(R.propEq("name", name))
  )(list);

export const prop = propName =>
  R.compose(
    K.Maybe.fromNullable,
    R.prop(propName)
  );
