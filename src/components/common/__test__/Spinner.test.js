import React from "react";
import Spinner from "../Spinner";
import { shallow } from "enzyme";

describe("<Spinner/>", () => {
  it("renders and matches snapshot", () => {
    const wrapper = shallow(<Spinner />);
    expect(wrapper).toMatchSnapshot();
  });
});
