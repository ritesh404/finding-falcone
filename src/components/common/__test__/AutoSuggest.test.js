import React from "react";
import AutoSuggest from "../AutoSuggest";
import { shallow } from "enzyme";
import * as R from "ramda";

describe("<AutoSuggest/>", () => {
  it("renders and matches snapshot", () => {
    const wrapper = shallow(
      <AutoSuggest
        title="Select a Planet"
        value={""}
        options={["Test1", "Test2", "Test3"]}
        onChange={R.id}
        selectedItem={""}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
