import React from "react";
import RadioList from "../RadioList";
import { shallow } from "enzyme";
import * as R from "ramda";

describe("<RadioList/>", () => {
  it("renders and matches snapshot", () => {
    const wrapper = shallow(
      <RadioList
        itemList={[
          { id: "item1", label: "item1" },
          { id: "item2", label: "item2" }
        ]}
        title={"Test"}
        onChange={R.id}
        selectedItem={"item1"}
        disableItem={() => false}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
