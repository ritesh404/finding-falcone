import ReactDOM from "react-dom";
import { lifecycle } from "recompose";
import { PORTAL_ID } from "../../lib/constants";

const modalRoot = document.getElementById(PORTAL_ID);

const modalWrapper = document.createElement("div");
modalWrapper.className = "modal";

// We handle modal lifcycle through recompose and keep the Modal component stateless
const withModal = lifecycle({
  componentDidMount() {
    modalRoot.appendChild(modalWrapper);
  },
  componentWillUnmount() {
    modalRoot.removeChild(modalWrapper);
  }
});

const Modal = withModal(({ children }) =>
  ReactDOM.createPortal(children, modalWrapper)
);

export default Modal;
