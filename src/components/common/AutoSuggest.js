import React from "react";
import * as R from "ramda";
import { withState, compose } from "recompose";

const enhance = compose(
  withState("focus", "setFocus", ({ autoFocus }) => !!autoFocus),
  withState("inputValue", "setInputValue", ({ value }) => value),
  withState("filteredOptions", "setFilteredOptions", ({ options }) => options)
);

const filterOptions = value =>
  R.filter(option => option.toLowerCase().indexOf(value.toLowerCase()) !== -1);

const renderOptions = (options, onOptionClick) =>
  R.map(
    option => (
      <div
        onMouseDown={e => {
          onOptionClick(option);
        }}
        key={option}
        className="auto-suggest-option"
      >
        {option}
      </div>
    ),
    options
  );

const AutoSuggest = ({
  title,
  value,
  inputValue,
  setInputValue,
  defaultValue,
  options,
  onChange,
  autoFocus,
  focus,
  setFocus,
  filteredOptions,
  setFilteredOptions
}) => (
  <div className="field">
    <label className="label">{title}</label>
    <div className="auto-suggest-wrapper">
      <div className="control has-icons-right">
        <input
          className="input"
          type="text"
          autoFocus={autoFocus}
          defaultValue={defaultValue}
          value={inputValue}
          onChange={e =>
            setInputValue(
              e.target.value,
              setFilteredOptions(filterOptions(e.target.value)(options))
            )
          }
          onFocus={() => setFocus(true)}
          onBlur={() => setFocus(false, setInputValue(value))}
        />
        <span className="icon is-small is-right">
          <i className="far fa-caret-square-down" />
        </span>
      </div>
      {focus ? (
        <div className="auto-suggest-options">
          {renderOptions(filteredOptions, onChange)}
        </div>
      ) : null}
    </div>
  </div>
);

export default enhance(AutoSuggest);
