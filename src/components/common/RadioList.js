import React from "react";
import * as R from "ramda";

const RadioList = ({
  itemList,
  title,
  onChange,
  selectedItem,
  disableItem
}) => (
  <div className="field">
    <label className="label">{title}</label>
    {R.map(
      item => (
        <div key={item.id} className="control">
          <label className="radio" disabled={disableItem(item)}>
            <input
              onChange={onChange}
              type="radio"
              name="answer"
              value={item.id}
              checked={selectedItem === item.id}
              disabled={disableItem(item)}
            />
            {item.label}
          </label>
        </div>
      ),
      itemList
    )}
  </div>
);

export default RadioList;
