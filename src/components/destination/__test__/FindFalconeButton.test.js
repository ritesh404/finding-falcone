import React from "react";
import FindFalconeButton from "../FindFalconeButton";
import { shallow } from "enzyme";
import * as R from "ramda";

describe("<FindFalconeButton/>", () => {
  it("renders and matches snapshot", () => {
    const wrapper = shallow(
      <FindFalconeButton onClick={R.id} disabled={false} />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
