import React from "react";
import SelectorCard from "../SelectorCard";
import { shallow } from "enzyme";
import * as R from "ramda";

describe("<SelectorCard/>", () => {
  it("renders and matches snapshot", () => {
    const wrapper = shallow(
      <SelectorCard title={"Test"} onClick={R.id} disabled={false} />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
