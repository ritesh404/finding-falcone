import React from "react";
import { TotalTimeTaken } from "../TotalTimeTaken";
import { shallow } from "enzyme";
import * as R from "ramda";

import { INITIAL_STATE } from "../../../lib/store";
import { vehicles, planets } from "../../../testData";

describe("<TotalTimeTaken/>", () => {
  it("renders and matches snapshot", () => {
    const NEW_STATE = { ...INITIAL_STATE, vehicles, planets };
    const wrapper = shallow(<TotalTimeTaken {...NEW_STATE} />);
    expect(wrapper).toMatchSnapshot();
  });
});
