import React from "react";
import { DestinationSelector } from "../DestinationSelector";
import { mount } from "enzyme";
import * as R from "ramda";
import { INITIAL_STATE } from "../../../lib/store";

describe("<DestinationSelector/>", () => {
  it("renders and matches snapshot", () => {
    const wrapper = mount(
      <DestinationSelector
        {...INITIAL_STATE}
        setSelectedDestination={R.id}
        disabled={false}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
