import React from "react";
import DestinationCard from "../DestinationCard";
import { shallow } from "enzyme";
import * as R from "ramda";

describe("<DestinationCard/>", () => {
  it("renders and matches snapshot", () => {
    const wrapper = shallow(
      <DestinationCard
        planet="donlon"
        vehicle="pod"
        onClick={R.id}
        disabled={false}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
