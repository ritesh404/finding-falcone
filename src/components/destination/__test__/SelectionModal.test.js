import React from "react";
import { SelectionModal } from "../SelectionModal";
import { shallow } from "enzyme";
import * as R from "ramda";
import K from "fp-kudojs";
import { INITIAL_STATE } from "../../../lib/store";

describe("<SelectionModal/>", () => {
  it("renders and matches snapshot", () => {
    const selectedVehicle = {
      name: "Space ship",
      total_no: 2,
      max_distance: 600,
      speed: 10
    };
    const selectedPlanet = { name: "Enchai", distance: 200 };
    const wrapper = shallow(
      <SelectionModal
        {...INITIAL_STATE}
        selectedVehicle={K.Maybe.Just(selectedVehicle)}
        selectedPlanet={K.Maybe.Just(selectedPlanet)}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
