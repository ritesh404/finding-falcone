import React from "react";
import { connect } from "react-redux";
import * as R from "ramda";
import K from "fp-kudojs";

import Modal from "../common/Modal";
import DestinationCard from "./DestinationCard";
import SelectorCard from "./SelectorCard";
import SelectionModal from "./SelectionModal";
import { renderMaybe } from "../../lib/common";
import { _setSelectedDestination } from "../../lib/actions";
import { STATE_KEYS } from "../../lib/store";

// If a destination is set then we render a card with the planet and vehicle info
// else we render a selector card
const renderCard = R.curry(
  (title, id, setSelectedDestination, destination, disabled) =>
    K.caseOf(
      {
        Just: ({ planet, vehicle }) => (
          <DestinationCard
            key={id}
            onClick={() => setSelectedDestination(K.Maybe.Just(id))}
            planet={planet}
            vehicle={vehicle}
            disabled={disabled}
          />
        ),
        Nothing: () => (
          <SelectorCard
            key={id}
            onClick={() => setSelectedDestination(K.Maybe.Just(id))}
            title={title}
            disabled={disabled}
          />
        )
      },
      destination
    )
);

const DestinationCards = ({
  setSelectedDestination,
  destinationList,
  disabled
}) => (
  <div className="destination-selector container">
    <h1 className="title">Select planets you want to search in</h1>
    <div className="columns is-flex-mobile">
      {R.map(
        destination =>
          renderCard(
            R.prop("label", destination),
            R.prop("id", destination),
            setSelectedDestination,
            R.prop("value", destination),
            disabled
          ),
        destinationList
      )}
    </div>
  </div>
);

export const DestinationSelector = props => {
  const {
    destination1,
    destination2,
    destination3,
    destination4,
    selectedDestination,
    setSelectedDestination,
    isFetchingPlanets,
    isFetchingVehicles,
    isFetchingToken,
    showError
  } = props;
  const destinationList = [
    { label: "Destination 1", id: "destination1", value: destination1 },
    { label: "Destination 2", id: "destination2", value: destination2 },
    { label: "Destination 3", id: "destination3", value: destination3 },
    { label: "Destination 4", id: "destination4", value: destination4 }
  ];
  return (
    <section className="section">
      <DestinationCards
        disabled={
          isFetchingPlanets ||
          isFetchingVehicles ||
          isFetchingToken ||
          showError
        }
        setSelectedDestination={setSelectedDestination}
        destinationList={destinationList}
      />
      {renderMaybe(
        destinationId => (
          <Modal>
            <SelectionModal
              destinationId={destinationId}
              destinationValue={props[destinationId]}
            />
          </Modal>
        ),
        selectedDestination
      )}
    </section>
  );
};

const mapStateToProps = R.pick([
  STATE_KEYS.destination1,
  STATE_KEYS.destination2,
  STATE_KEYS.destination3,
  STATE_KEYS.destination4,
  STATE_KEYS.isFetchingPlanets,
  STATE_KEYS.isFetchingVehicles,
  STATE_KEYS.isFetchingToken,
  STATE_KEYS.selectedDestination,
  STATE_KEYS.showError
]);

const mapDispatchToProps = (dispatch, ownProps) => ({
  setSelectedDestination: K.compose(
    dispatch,
    _setSelectedDestination
  )
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DestinationSelector);
