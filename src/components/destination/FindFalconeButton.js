import React from "react";

const FindFalconeButton = ({ disabled, onClick }) => (
  <div className="section">
    <button
      onClick={onClick}
      className="button is-large is-primary"
      disabled={disabled}
    >
      Find Falcone!
    </button>
  </div>
);

export default FindFalconeButton;
