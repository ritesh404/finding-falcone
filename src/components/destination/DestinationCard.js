import React from "react";
import * as R from "ramda";

import Donlon from "../planets/Donlon";
import Enchai from "../planets/Enchai";
import Jebing from "../planets/Jebing";
import Lerbin from "../planets/Lerbin";
import Pingasor from "../planets/Pingasor";
import Sapir from "../planets/Sapir";

const DestinationCard = ({ planet, vehicle, onClick, disabled }) => (
  <div
    onClick={disabled ? R.identity : onClick}
    className={`selector-card column ${disabled ? "disabled" : "active"}`}
  >
    <div className="destination-planet-name">
      Destination: <span className="planet-name">{planet}</span>
    </div>
    <div className="destination-planet-svg">{getPlanetSvg(planet)}</div>
    <div className="destination-vehicle">
      <span className={`vehicle-img ${getVehicleClassName(vehicle)}`} />
      <div className="vehicle-name-container">
        <div>Vehicle:</div>
        <div className="vehicle-name">{vehicle}</div>
      </div>
    </div>
  </div>
);

const getPlanetSvg = planet => {
  switch (planet.toLowerCase()) {
    case "donlon":
      return <Donlon />;
    case "enchai":
      return <Enchai />;
    case "jebing":
      return <Jebing />;
    case "lerbin":
      return <Lerbin />;
    case "pingasor":
      return <Pingasor />;
    case "sapir":
      return <Sapir />;
    default:
      return <Donlon />;
  }
};

const getVehicleClassName = vehicle => vehicle.toLowerCase().replace(" ", "-");

export default DestinationCard;
