import React from "react";
import * as R from "ramda";

const SelectorCard = ({ title, onClick, disabled }) => (
  <div
    onClick={disabled ? R.identity : onClick}
    className={`selector-card column ${disabled ? "disabled" : "active"}`}
  >
    <div className="card-btn">
      <i className="fas fa-plus-circle" />
    </div>
    <div>{title}</div>
  </div>
);

export default SelectorCard;
