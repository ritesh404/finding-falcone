import React from "react";
import K from "fp-kudojs";
import * as R from "ramda";
import { connect } from "react-redux";
import { STATE_KEYS } from "../../lib/store";
import { findWithName, prop } from "../../lib/common";

const time = K.compose(
  K.liftA2,
  K.curry
)((speed, distance) => distance.getValue() / speed.getValue());

const timeTakenForDestination = R.curry((vehicles, planets, destination) =>
  time(
    K.fmap(prop("speed"), findWithName(destination.vehicle, vehicles)),
    K.fmap(prop("distance"), findWithName(destination.planet, planets))
  ).getValue()
);

export const TotalTimeTaken = ({
  vehicles,
  planets,
  destination1,
  destination2,
  destination3,
  destination4
}) => {
  const destinations = [destination1, destination2, destination3, destination4];
  const totalTime = R.compose(
    R.sum,
    K.Maybe.catMaybes,
    R.map(K.fmap(timeTakenForDestination(vehicles, planets)))
  )(destinations);

  return (
    <div className="container">
      <h2 className="subtitle">{`Total Time Taken: ${totalTime}`}</h2>
    </div>
  );
};

const mapStateToProps = R.pick([
  STATE_KEYS.vehicles,
  STATE_KEYS.planets,
  STATE_KEYS.destination1,
  STATE_KEYS.destination2,
  STATE_KEYS.destination3,
  STATE_KEYS.destination4
]);

export default connect(
  mapStateToProps,
  null
)(TotalTimeTaken);
