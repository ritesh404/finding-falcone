import React from "react";
import * as R from "ramda";
import K from "fp-kudojs";
import { connect } from "react-redux";
import { withState, compose, withHandlers } from "recompose";

import AutoSuggest from "../common/AutoSuggest";
import RadioList from "../common/RadioList";
import { renderMaybe } from "../../lib/common";
import { STATE_KEYS } from "../../lib/store";
import { findWithName } from "../../lib/common";
import { _setDestination, _setSelectedDestination } from "../../lib/actions";

const getName = R.prop("name");

const extractNames = R.map(getName);

const availablePlanets = R.curry((planets, destinations) => {
  const selectedPlanets = R.compose(
    R.map(R.prop("planet")),
    K.Maybe.catMaybes
  )(destinations);

  return R.filter(
    planet => !R.contains(R.prop("name", planet), selectedPlanets),
    planets
  );
});

const availableVehicles = R.curry((vehicles, destinations) => {
  const selectedVehicles = R.compose(
    R.map(R.prop("vehicle")),
    K.Maybe.catMaybes
  )(destinations);

  return R.map(
    vehicle => ({
      ...vehicle,
      used: R.filter(R.equals(R.prop("name", vehicle)), selectedVehicles).length
    }),
    vehicles
  );
});

const planetList = R.compose(
  extractNames,
  availablePlanets
);

const availableVehicleCount = vehicle => vehicle.total_no - vehicle.used;

const vehicleList = R.compose(
  R.map(vehicle => ({
    ...vehicle,
    id: vehicle.name,
    label: ` ${vehicle.name} (${availableVehicleCount(vehicle)})`
  })),
  availableVehicles
);

const destinationPropValue = (prop, destinationValue) =>
  destinationValue
    ? K.fmap(destination => destination[prop], destinationValue)
    : K.Maybe.Nothing();

const selectedPlanetToText = K.caseOf({
  Just: K.id,
  Nothing: K.constant("")
});

const enhance = compose(
  withState("selectedPlanet", "setPlanet", ({ destinationValue }) =>
    destinationPropValue("planet", destinationValue)
  ),
  withState("selectedVehicle", "setVehicle", ({ destinationValue }) =>
    destinationPropValue("vehicle", destinationValue)
  ),
  withHandlers({
    onPlanetChange: props => value => {
      props.setPlanet(
        R.contains(value, extractNames(props.planets))
          ? K.Maybe.Just(value)
          : K.Maybe.Nothing()
      );
    },
    onVehicleChange: props => event => {
      const value = event.target.value;
      props.setVehicle(
        R.contains(value, extractNames(props.vehicles))
          ? K.Maybe.Just(value)
          : K.Maybe.Nothing()
      );
    }
  })
);

// A vehicle should be disabled if its max distance is less than that of the planet
// or if all of the vehicle has been used up unless it is used in the current destination
const disableVehicle = (vehicle, planet, destination) =>
  (availableVehicleCount(vehicle) === 0 &&
    destinationPropValue("vehicle", destination).getValue() !== vehicle.id) ||
  planet.distance > vehicle.max_distance;

const _SelectionModal = ({
  planets,
  vehicles,
  destination1,
  destination2,
  destination3,
  destination4,
  destinationId,
  destinationValue,
  selectedPlanet,
  selectedVehicle,
  onPlanetChange,
  onVehicleChange,
  setDestination,
  onClose
}) => {
  const allDestinations = [
    destination1,
    destination2,
    destination3,
    destination4
  ];

  return (
    <div className="modal-content">
      <div className="section">
        <AutoSuggest
          title="Select a Planet"
          value={selectedPlanetToText(selectedPlanet)}
          options={planetList(planets, allDestinations)}
          onChange={onPlanetChange}
          selectedItem={selectedPlanet.getValue()}
        />
        {renderMaybe(
          planet => (
            <React.Fragment>
              <RadioList
                title="Select a Vehicle"
                itemList={vehicleList(vehicles, allDestinations)}
                onChange={onVehicleChange}
                selectedItem={selectedVehicle.getValue()}
                disableItem={vehicle =>
                  disableVehicle(
                    vehicle,
                    findWithName(planet, planets).getValue(),
                    destinationValue
                  )
                }
              />
              {renderMaybe(
                vehicle => (
                  <div className="tile">
                    <button
                      onClick={() =>
                        setDestination(destinationId, planet, vehicle)
                      }
                      className="button is-primary"
                    >
                      Set Course!
                    </button>
                  </div>
                ),
                selectedVehicle
              )}
            </React.Fragment>
          ),
          selectedPlanet
        )}
        <button
          onClick={onClose}
          className="modal-close is-large"
          aria-label="close"
        />
      </div>
    </div>
  );
};

export const SelectionModal = enhance(_SelectionModal);

const mapStateToProps = R.pick([
  STATE_KEYS.planets,
  STATE_KEYS.vehicles,
  STATE_KEYS.destination1,
  STATE_KEYS.destination2,
  STATE_KEYS.destination3,
  STATE_KEYS.destination4
]);

const _closeModalAction = K.compose(
  K.constant,
  _setSelectedDestination
)(K.Maybe.Nothing());

const mapDispatchToProps = dispatch => ({
  onClose: () => dispatch(_closeModalAction()),
  setDestination: K.compose(
    dispatch,
    _closeModalAction,
    dispatch,
    _setDestination
  )
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectionModal);
