import React from "react";
import Lerbin from "../Lerbin";
import { shallow } from "enzyme";

describe("<Lerbin/>", () => {
  it("renders and matches snapshot", () => {
    const wrapper = shallow(<Lerbin />);
    expect(wrapper).toMatchSnapshot();
  });
});
