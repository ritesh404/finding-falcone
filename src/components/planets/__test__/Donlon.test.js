import React from "react";
import Donlon from "../Donlon";
import { shallow } from "enzyme";

describe("<Donlon/>", () => {
  it("renders and matches snapshot", () => {
    const wrapper = shallow(<Donlon />);
    expect(wrapper).toMatchSnapshot();
  });
});
