import React from "react";
import Sapir from "../Sapir";
import { shallow } from "enzyme";

describe("<Sapir/>", () => {
  it("renders and matches snapshot", () => {
    const wrapper = shallow(<Sapir />);
    expect(wrapper).toMatchSnapshot();
  });
});
