import React from "react";
import Pingasor from "../Pingasor";
import { shallow } from "enzyme";

describe("<Pingasor/>", () => {
  it("renders and matches snapshot", () => {
    const wrapper = shallow(<Pingasor />);
    expect(wrapper).toMatchSnapshot();
  });
});
