import React from "react";
import Jebing from "../Jebing";
import { shallow } from "enzyme";

describe("<Jebing/>", () => {
  it("renders and matches snapshot", () => {
    const wrapper = shallow(<Jebing />);
    expect(wrapper).toMatchSnapshot();
  });
});
