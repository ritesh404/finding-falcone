import React from "react";

const Sapir = () => (
  <svg
    className="sapir planet"
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 105.833 105.833"
  >
    <defs>
      <ellipse
        id="sapirClip"
        cx="53.05"
        cy="244.106"
        rx="52.027"
        ry="52.138"
        fill="#dc6f4b"
        fillOpacity=".957"
      />
      <clipPath id="sapirClipPath">
        <use xlinkHref="#sapirClip" overflow="visible" />
      </clipPath>
    </defs>
    <g transform="translate(0 -191.167)" clipPath="url(#sapirClipPath)">
      <use xlinkHref="#sapirClip" fill="#4a96c2" overflow="visible" />
      <g className="sapir-lines" fill="#bc5d42">
        <g>
          <rect
            width="21.545"
            height="5.292"
            x="27.214"
            y="212.711"
            ry="2.646"
          />
          <rect
            ry="2.646"
            y="242.913"
            x="74.254"
            height="5.292"
            width="21.545"
          />
          <rect
            width="21.545"
            height="5.292"
            x="25.343"
            y="274.183"
            ry="2.646"
          />
          <rect width="21.01" height="1.817" x="44.854" y="230.886" ry=".909" />
          <rect width="6.31" height="3.688" x="30.154" y="242.378" ry="1.844" />
          <rect
            ry="1.844"
            y="254.405"
            x="52.872"
            height="3.688"
            width="12.19"
          />
          <rect
            ry=".909"
            y="207.099"
            x="63.563"
            height="1.817"
            width="13.527"
          />
          <rect
            width="17.536"
            height="4.223"
            x="72.383"
            y="266.967"
            ry="2.111"
          />
          <rect
            width="16.734"
            height="2.619"
            x="19.463"
            y="256.811"
            ry="1.309"
          />
          <rect ry="1.71" y="222.333" x="79.065" height="3.421" width="8.181" />
          <rect
            width="10.052"
            height="3.688"
            x="14.92"
            y="231.955"
            ry="1.844"
          />
          <rect ry="1.844" y="241.309" x="-.314" height="3.688" width="6.043" />
          <rect
            ry="1.309"
            y="283.805"
            x="87.884"
            height="2.619"
            width="17.536"
          />
        </g>
        <g transform="translate(107.222 -.168)">
          <rect
            ry="2.646"
            y="212.711"
            x="27.214"
            height="5.292"
            width="21.545"
          />
          <rect
            width="21.545"
            height="5.292"
            x="74.254"
            y="242.913"
            ry="2.646"
          />
          <rect
            ry="2.646"
            y="274.183"
            x="25.343"
            height="5.292"
            width="21.545"
          />
          <rect ry=".909" y="230.886" x="44.854" height="1.817" width="21.01" />
          <rect ry="1.844" y="242.378" x="30.154" height="3.688" width="6.31" />
          <rect
            width="12.19"
            height="3.688"
            x="52.872"
            y="254.405"
            ry="1.844"
          />
          <rect
            width="13.527"
            height="1.817"
            x="63.563"
            y="207.099"
            ry=".909"
          />
          <rect
            ry="2.111"
            y="266.967"
            x="72.383"
            height="4.223"
            width="17.536"
          />
          <rect
            ry="1.309"
            y="256.811"
            x="19.463"
            height="2.619"
            width="16.734"
          />
          <rect width="8.181" height="3.421" x="79.065" y="222.333" ry="1.71" />
          <rect
            ry="1.844"
            y="231.955"
            x="14.92"
            height="3.688"
            width="10.052"
          />
          <rect width="6.043" height="3.688" x="-.314" y="241.309" ry="1.844" />
          <rect
            width="17.536"
            height="2.619"
            x="87.884"
            y="283.805"
            ry="1.309"
          />
        </g>
        <g transform="translate(213.491 -.168)">
          <rect
            width="21.545"
            height="5.292"
            x="27.214"
            y="212.711"
            ry="2.646"
          />
          <rect
            ry="2.646"
            y="242.913"
            x="74.254"
            height="5.292"
            width="21.545"
          />
          <rect
            width="21.545"
            height="5.292"
            x="25.343"
            y="274.183"
            ry="2.646"
          />
          <rect width="21.01" height="1.817" x="44.854" y="230.886" ry=".909" />
          <rect width="6.31" height="3.688" x="30.154" y="242.378" ry="1.844" />
          <rect
            ry="1.844"
            y="254.405"
            x="52.872"
            height="3.688"
            width="12.19"
          />
          <rect
            ry=".909"
            y="207.099"
            x="63.563"
            height="1.817"
            width="13.527"
          />
          <rect
            width="17.536"
            height="4.223"
            x="72.383"
            y="266.967"
            ry="2.111"
          />
          <rect
            width="16.734"
            height="2.619"
            x="19.463"
            y="256.811"
            ry="1.309"
          />
          <rect ry="1.71" y="222.333" x="79.065" height="3.421" width="8.181" />
          <rect
            width="10.052"
            height="3.688"
            x="14.92"
            y="231.955"
            ry="1.844"
          />
          <rect ry="1.844" y="241.309" x="-.314" height="3.688" width="6.043" />
          <rect
            ry="1.309"
            y="283.805"
            x="87.884"
            height="2.619"
            width="17.536"
          />
        </g>
      </g>
      <path
        d="M95.159 213.594a65.823 60.264 0 0 1 2.36 15.96 65.823 60.264 0 0 1-65.824 60.265 65.823 60.264 0 0 1-3.953-.182 52.027 52.138 0 0 0 25.309 6.607 52.027 52.138 0 0 0 52.026-52.138 52.027 52.138 0 0 0-9.918-30.512z"
        fillOpacity=".276"
      />
    </g>
  </svg>
);

export default Sapir;
