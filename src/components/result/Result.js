import React from "react";
import K from "fp-kudojs";
import * as R from "ramda";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import { STATE_KEYS } from "../../lib/store";

// This component renders the result of the search
// If no result is present then it renders text link to the index route
export const Result = ({ result }) => (
  <section className="hero is-medium">
    <div className="hero-body">
      <div className="container">
        {K.caseOf(
          {
            Just: msg => msg,
            Nothing: () => <Link to="/find">Click here to Find Falcone!</Link>
          },
          getResultText(result)
        )}
      </div>
    </div>
  </section>
);

// Returns a Maybe type. Based on the result success or failure it renders appropriate message.
// this message is wrapped in a "Just"
// If no result has been fetched returns a "Nothing"
const getResultText = K.fmap(result =>
  result.status === "success" ? (
    <React.Fragment>
      <h1 className="title">Success!</h1>
      <h2 className="subtitle">
        Congratulations on Finding Falcone. King Shan is mighty pleased
      </h2>
    </React.Fragment>
  ) : (
    <React.Fragment>
      <h1 className="title">Mission Failed!!</h1>
      <h2 className="subtitle">
        Looks like Falcone escaped. King Shan will not be pleased..
        <br />
        <Link to="/find">Click here to try again</Link>
      </h2>
    </React.Fragment>
  )
);

const mapStateToProps = R.pick([STATE_KEYS.result]);

export default connect(
  mapStateToProps,
  null
)(Result);
