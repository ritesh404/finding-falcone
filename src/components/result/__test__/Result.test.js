import React from "react";
import K from "fp-kudojs";
import { Result } from "../Result";
import { shallow } from "enzyme";

import { INITIAL_STATE } from "../../../lib/store";

describe("<Result/>", () => {
  it("renders when no result is supplied with link to index", () => {
    const wrapper = shallow(<Result {...INITIAL_STATE} />);
    expect(wrapper).toMatchSnapshot();
  });

  it("renders on success", () => {
    const wrapper = shallow(
      <Result {...INITIAL_STATE} result={K.Maybe.Just({ status: "success" })} />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("renders on failure", () => {
    const wrapper = shallow(
      <Result {...INITIAL_STATE} result={K.Maybe.Just({ status: "failed" })} />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
