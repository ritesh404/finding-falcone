import React from "react";
import { Link } from "react-router-dom";

const Welcome = () => (
  <section className="section">
    <div className="container">
      <div className="falcone-img" />
      <h1 className="title">A long time ago in a galaxy far, far away....</h1>
      <p>
        In the planet of Lengaburu…in the distant galaxy of Tara B. After the
        recent war with neighbouring planet Falicornia, King Shan has exiled the
        Queen of Falicornia for 15 years.
      </p>
      <br />
      <p>
        Queen Al Falcone is now in hiding. But if King Shan can find her before
        the years are up, she will be exiled for another 15 years…
      </p>
      <br />
      <p>
        King Shan has received intelligence that Al Falcone is in hiding in one
        of these 6 planets - DonLon, Enchai, Jebing, Sapir, Lerbin & Pingasor.
        However he has limited resources at his disposal & can send his army to
        only 4 of these planets.{" "}
      </p>
      <br />
      <p>
        Your mission, should you choose to accept it, is to help King Shan and
        &nbsp;
        <span className="has-text-weight-bold">Find Falcone!</span>
      </p>
      <br />

      <Link to="/find" className="button is-large is-primary">
        Click Here To Start
      </Link>
    </div>
  </section>
);

export default Welcome;
