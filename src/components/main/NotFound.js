import React from "react";

import { Link } from "react-router-dom";

const NotFound = () => (
  <section className="hero is-medium">
    <div className="hero-body">
      <div className="container">
        <h1 className="title">404</h1>
        <h2 className="subtitle">Looks like you lost your way..</h2>
        <Link to="/">Click here to Find Falcone!</Link>
      </div>
    </div>
  </section>
);

export default NotFound;
