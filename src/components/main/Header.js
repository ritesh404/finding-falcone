import React from "react";
import * as R from "ramda";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Spinner from "../common/Spinner";
import { STATE_KEYS } from "../../lib/store";

export const Header = ({
  isFetchingVehicles,
  isFetchingPlanets,
  isFetchingToken,
  isFetchingResult
}) => (
  <header className="app-header">
    <nav className="navbar" role="navigation" aria-label="main navigation">
      <div className="navbar-start">
        <div className="navbar-item">
          <Link to="/">Finding Falcone</Link>
        </div>
      </div>
      <div className="navbar-end">
        <div className="navbar-item">
          {R.any(R.equals(true), [
            isFetchingVehicles,
            isFetchingPlanets,
            isFetchingToken,
            isFetchingResult
          ]) && <Spinner />}
        </div>
      </div>
    </nav>
  </header>
);

const mapStateToProps = R.pick([
  STATE_KEYS.isFetchingVehicles,
  STATE_KEYS.isFetchingPlanets,
  STATE_KEYS.isFetchingToken,
  STATE_KEYS.isFetchingResult
]);

export default connect(
  mapStateToProps,
  null
)(Header);
