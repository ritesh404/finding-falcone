import React from "react";
import { Header } from "../Header";
import { shallow } from "enzyme";
import { INITIAL_STATE } from "../../../lib/store";

describe("<Header/>", () => {
  it("renders and matches snapshot", () => {
    const wrapper = shallow(<Header {...INITIAL_STATE} />);
    expect(wrapper).toMatchSnapshot();
  });
});
