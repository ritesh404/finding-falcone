import React from "react";
import { Main } from "../Main";
import { shallow } from "enzyme";
import { INITIAL_STATE } from "../../../lib/store";

describe("<Main/>", () => {
  const props = {
    fetchToken: jest.fn(),
    fetchVehicles: jest.fn(),
    fetchPlanets: jest.fn()
  };
  it("renders and matches snapshot", () => {
    const wrapper = shallow(<Main {...INITIAL_STATE} {...props} />);
    expect(wrapper).toMatchSnapshot();
  });

  it("fetches token, vheicles and planets on componentDidMount", () => {
    const wrapper = shallow(<Main {...INITIAL_STATE} {...props} />);
    wrapper.instance().componentDidMount();
    expect(props.fetchToken).toBeCalled();
    expect(props.fetchVehicles).toBeCalled();
    expect(props.fetchPlanets).toBeCalled();
  });
});
