import React from "react";
import Welcome from "../Welcome";
import { shallow } from "enzyme";

describe("<Welcome/>", () => {
  it("renders and matches snapshot", () => {
    const wrapper = shallow(<Welcome />);
    expect(wrapper).toMatchSnapshot();
  });
});
