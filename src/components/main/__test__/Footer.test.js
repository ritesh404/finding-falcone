import React from "react";
import Footer from "../Footer";
import { shallow } from "enzyme";

describe("<Footer/>", () => {
  it("renders and matches snapshot", () => {
    const wrapper = shallow(<Footer />);
    expect(wrapper).toMatchSnapshot();
  });
});
