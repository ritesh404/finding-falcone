import React from "react";

const Footer = () => (
  <footer className="footer">
    <div className="content has-text-centered">
      <p>
        <strong>Coding Challenge</strong> by
        <a href="https://www.geektrust.in/coding-problem"> Geektrust</a>.
      </p>
    </div>
  </footer>
);

export default Footer;
