import React from "react";
import * as R from "ramda";
import K from "fp-kudojs";
import { connect } from "react-redux";
import { lifecycle } from "recompose";

import DestinationSelector from "../destination/DestinationSelector";
import FindFalconeButton from "../destination/FindFalconeButton";
import TotalTimeTaken from "../destination/TotalTimeTaken";
import {
  _fetchVehicles,
  _fetchPlanets,
  _fetchToken,
  _fetchResult
} from "../../lib/actions";
import { STATE_KEYS } from "../../lib/store";

// Fetch the token, vehicles and planets list on main component mount
const enhance = lifecycle({
  componentDidMount() {
    this.props.fetchToken();
    this.props.fetchVehicles();
    this.props.fetchPlanets();
  }
});

const isAllDestinationSet = R.all(K.Maybe.isJust);

const _Main = ({
  showError,
  token,
  destination1,
  destination2,
  destination3,
  destination4,
  isFetchingResult,
  fetchResult
}) => (
  <React.Fragment>
    {showError && <ErrorBar />}
    <DestinationSelector />
    <TotalTimeTaken />
    <FindFalconeButton
      onClick={fetchResult}
      disabled={
        !isAllDestinationSet([
          destination1,
          destination2,
          destination3,
          destination4
        ]) ||
        token.isNothing() ||
        isFetchingResult
      }
    />
  </React.Fragment>
);

const ErrorBar = () => (
  <div className="error-box">Something went wrong. Please try again later</div>
);

const mapStateToProps = R.pick([
  STATE_KEYS.showError,
  STATE_KEYS.destination1,
  STATE_KEYS.destination2,
  STATE_KEYS.destination3,
  STATE_KEYS.destination4,
  STATE_KEYS.token,
  STATE_KEYS.isFetchingResult
]);

const mapDispatchToProps = dispatch => ({
  fetchToken: () => dispatch(_fetchToken()),
  fetchVehicles: () => dispatch(_fetchVehicles()),
  fetchPlanets: () => dispatch(_fetchPlanets()),
  fetchResult: () => dispatch(_fetchResult())
});

export const Main = enhance(_Main);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
