import React from "react";
import { Router, Route, Switch } from "react-router-dom";

import Welcome from "./main/Welcome";
import Main from "./main/Main";
import Result from "./result/Result";
import Header from "./main/Header";
import Footer from "./main/Footer";
import NotFound from "./main/NotFound";
import appHistory from "../lib/history";

// App component renders views based on route
const App = () => {
  return (
    <Router history={appHistory}>
      <div className="app">
        <Header />
        <Switch>
          <Route exact path="/" component={Welcome} />
          <Route exact path="/find" component={Main} />
          <Route exact path="/result" component={Result} />
          <Route component={NotFound} />
        </Switch>
        <Footer />
      </div>
    </Router>
  );
};

export default App;
